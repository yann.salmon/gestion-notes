#!/usr/bin/env python3

import sys
import argparse
import os
import re
import subprocess

from bdd import BDD_SQLite
from moteur import CreerDS, Initialiser, CreerQCM, DS_base, ListeDS
from interf_clav import InterfaceClavier
from interf_serie import InterfaceSerie
from interf_batch import InterfaceBatch
from interf_csv import InterfaceCSV
from rapports import Rapport, Fiches
from pronote import Pronote, Bulletin, ECTS
from git import Gitpull, Gitpush
from qcm import GenererQCM, ImporterReponsesQCM, NoterQCM
import os

import tech

## Interface


def defaultBasedir(nf_bdd) :
    return os.path.dirname(os.path.realpath(nf_bdd))

reg=re.compile('^[A-Za-z0-9*_]+$')

def valide(s) :
    return reg.match(s)


def sanitize(args) :
    if not valide(args.tbl_etu) or (hasattr(args, "tbl_ds") and not valide(args.tbl_ds)) :
        raise ValueError("Mauvais nom de table")
    if args.tbl_etu == "classes_ds" :
        raise ValueError("Nom interdit pour la table des étudiants")
    if hasattr(args, "doublon") :
        args.doublon = int(args.doublon)
        if args.doublon not in range(0, 4) :
            raise ValueError("Action par défaut en cas de doublon invalide")
    if os.path.isfile(args.base) and not os.access(args.base, os.W_OK) :
        raise ValueError("BDD non inscriptible")
    if hasattr(args, "classes") :
        for x in args.classes :
            if not valide(x) :
                raise ValueError('Mauvais nom de classe')
    if hasattr(args, "options") and args.options is not None :
        for x in args.options :
            if not valide(x) :
                raise ValueError("Mauvais nom d'option")
    if hasattr(args, "preambule") :
        if args.preambule is None :
            args.preambule = os.path.join(defaultBasedir(args.base), "preambule_"+args.commande+".tex")
        else :
            if not os.path.isfile(args.preambule) :
                raise ValueError("Le préambule doit être un fichier existant")
    if hasattr(args, "basedir") :
        if args.basedir is None :
            args.basedir = os.path.join(defaultBasedir(args.base), os.path.basename(os.path.realpath(args.base))+"_qcm")
    if hasattr(args, "ax") and args.ax != "csv" :
        autorise = [3, 4, 5, 6]
        if len(args.ax) > 1 and args.ax[0].lower() == "a" :
            args.ax = args.ax[1:]
        if len(args.ax) > 1 and args.ax[-1].lower() == "m" :
            verif = args.ax[:-1]
        else :
            verif = args.ax
        try :
            if int(verif) not in autorise :
                raise ValueError("Mauvais ax. Valeurs permises : " + ", ".join(autorise))
        except :
            raise ValueError("Mauvais ax. Valeurs permises : " + ", ".join(autorise))

    if not hasattr(args, "anon") :
        args.anon = False



def traiter(args) :
    print("Ouverture de la base " + args.base)
    sanitize(args)

    if args.commande == "gitpull" :
        Gitpull(args)
    elif args.commande == "gitpush" :
        Gitpush(args)
    else :
        args.bdd = BDD_SQLite(args.base, anon=args.anon)
        if args.commande == "listeds" :
            ListeDS(args)
        elif args.commande == "corriger" :
            if hasattr(args, "batch") and args.batch is not None :
                InterfaceBatch(args).Corriger()
            elif hasattr(args, "csv") and args.csv is not None :
                InterfaceCSV(args).Corriger()
            else :
                if hasattr(args, "serie") and args.serie :
                    interf = InterfaceSerie
                else :
                    interf = InterfaceClavier
                interf(args).Corriger()
        elif args.commande == "manquantes":
            cm, qcmm = DS_base(args).CopiesQCMManquants()
            if len(cm) == 0 :
                print("Aucune copie manquante")
            else:
                print(len(cm), "copies manquantes")
                if len(cm) <= 5 :
                    for c in cm :
                        print(list(c))
            if qcmm is not None :
                if len(qcmm) == 0 :
                    print("Aucun QCM manquant")
                else :
                    print(len(qcmm), "QCM manquants")
                    for l in qcmm :
                        print(list(l))
        elif args.commande == "fiches" :
            NoterQCM(args)
            Fiches(args)
        elif args.commande == "pronote" :
            args.mult = float(args.mult)
            args.add = float(args.add)
            Pronote(args)
        elif args.commande == "bulletin" :
            Bulletin(args)
        elif args.commande == "ects" :
            ECTS(args)
        elif args.commande == "rapport" :
            NoterQCM(args)
            Rapport(args)
        elif args.commande == "creer" :
            CreerDS(args)
        elif args.commande == "creerqcm" :
            CreerQCM(args)
        elif args.commande == "init" :
            Initialiser(args)
        elif args.commande == "genqcm" :
            GenererQCM(args)
        elif args.commande == "repqcm" :
            ImporterReponsesQCM(args)
        else :
            ValueError("ne devrait pas se produire")

def main() :
    tech.get_config()
    try :
        defaultDir = os.path.join(subprocess.check_output(["xdg-user-dir", "DOCUMENTS"], universal_newlines=True).strip(), "gestion-notes")
    except :
        defaultDir = os.path.dirname(sys.argv[0])

    parser = argparse.ArgumentParser()
    ## options communes
    fonda = argparse.ArgumentParser(add_help=False)
    fonda.add_argument("--base", help="Fichier de la BDD", default=tech.config.get("defaultBase", os.path.join(defaultDir, "base.sqlite")))
    fonda.add_argument("--tbl-etu", help="Nom de la table des étudiants", default="etudiants", dest="tbl_etu")
    fonda.add_argument("--debug", help="Active le mode debug (garde les fichiers temporaires).", action="store_true")

    common = argparse.ArgumentParser(add_help=False, parents=[fonda])
    common.add_argument("tbl_ds", help="Nom de la table de DS")

    ## gestion des commandes
    subparsers = parser.add_subparsers(dest="commande", title="Opérations", description="Opérations possibles")
    ## liste des devoirs
    parser_listeds = subparsers.add_parser("listeds", parents=[fonda], help="Donne la liste des DS")

    ## corriger
    parser_corriger = subparsers.add_parser("corriger", parents=[common], help="Lance une session de correction")
    parser_corriger.add_argument("--batch", help="Traiter les notes depuis un fichier")
    parser_corriger.add_argument("--doublon", help="Action à effectuer en cas de doublon", default=0)
    parser_corriger.add_argument("--csv", help="Importer un tableau de notes en CSV")
    parser_corriger.add_argument("--serie", help="Corriger question par question", action="store_true")
    parser_corriger.add_argument("--debut", help="Numéro de la première question à corriger. Ignoré en mode série, batch et csv.", default=None)
    parser_corriger.add_argument("--fin", help="Numéro de la dernière question à corriger. Ignoré en mode série, batch et csv.", default=None)
    parser_corriger.add_argument("--no-comment", help="Supprime le prompt pour un commentaire en fin de copie.", action="store_true", default=False)

    ## copies manquantes
    parser_manquantes = subparsers.add_parser("manquantes", parents=[common], help="Donne la liste des copies manquantes")

    ## QCM
    parser_creerqcm = subparsers.add_parser("creerqcm", parents=[common], help="Crée les tables pour gérer le QCM d'un DS")

    parser_genqcm = subparsers.add_parser("genqcm", parents=[common], help="Génère les PDF de réponses pour le QCM d'un DS")
    parser_genqcm.add_argument("--preambule", help="Fichier tex à inclure dans le préambule")
    parser_genqcm.add_argument("--basedir", help="Répertoire de rangement des projets SDAPS", default=None)
    parser_genqcm.add_argument("--deldir", help="Effacer automatiquement le dossier SDAPS", action="store_true")
    parser_genqcm.add_argument("--ax", help="Taille du papier A3, 4, 5 ou 6", default = tech.config.get("defaultQCMpaper", "4"))

    parser_importqcm = subparsers.add_parser("repqcm", parents=[common], help="Importe des feuilles de réponses à un QCM")
    parser_importqcm.add_argument("--basedir", help="Répertoire de rangement des projets SDAPS", default=None)
    parser_importqcm.add_argument("fichiers", nargs="+", help="Nom du fichier (image, PDF) contenant les données")
    parser_importqcm.add_argument("--convert", help="Conversion auto du fichier (utilise --convert de SDAPS)", action="store_true")

   ## rapports et fiches
    rapports = argparse.ArgumentParser(add_help=False, parents=[common])
    rapports.add_argument("--anon", help="Rapports et fiches anonymes", action="store_true")

    ## fiches
    parser_fiches = subparsers.add_parser("fiches", parents=[rapports], help="Génère les fiches étudiants")
    parser_fiches.add_argument("--classe", nargs="+", help="Classe(s) pour laquelle générer les fiches")
    parser_fiches.add_argument("--groupe", help="Groupe pour lequel générer les fiches")
    parser_fiches.add_argument("--stat-groupe", help="Afficher les statistiques par groupe sur les fiches", action="store_true")
    parser_fiches.add_argument("--preambule", help="Fichier tex à inclure dans le préambule")
    parser_fiches.add_argument("--elaguer", help="Ne pas inclure les fiches sans note", action="store_true")
    parser_fiches.add_argument("--indiv", help="Générer des fiches individuelles", action="store_true")

    ## rapport
    parser_rapport = subparsers.add_parser("rapport", parents=[rapports], help="Génère le rapport général")
    parser_rapport.add_argument("--preambule", help="Fichier tex à inclure dans le préambule")

    ## pronote
    parser_pronote = subparsers.add_parser("pronote", parents=[common], help="Envoie les notes vers Pronote")
    parser_pronote.add_argument("classe", help="Classe concernée")
    parser_pronote.add_argument("--defaut", help="Code à envoyer en cas d'absence de note", default="Z")
    parser_pronote.add_argument("--mult", help="Coefficient multiplicateur (appliqué en premier)", default=1.0)
    parser_pronote.add_argument("--add", help="Addition (appliqué en deuxième", default=0.0)
    parser_pronote.add_argument("--noms-suppl", nargs="+", help="Noms à sauter dans la liste", default=[])


    ## bulletin
    parser_bulletin = subparsers.add_parser("bulletin", parents=[fonda], help="Envoie les commentaires vers Pronote")
    parser_bulletin.add_argument('periode', help="Semestre S1 ou S2")
    parser_bulletin.add_argument("classe", help="Classe concernée")
    parser_bulletin.add_argument('--option', help="Option concernée")
    parser_bulletin.add_argument("--noms-suppl", nargs="+", help="Noms à sauter dans la liste", default=[])

    ## ects
    parser_ects = subparsers.add_parser("ects", parents=[fonda], help="Envoie les lettres ECTS")
    parser_ects.add_argument('periode', help="Semestre S1 ou S2")
    parser_ects.add_argument("classe", help="Classe concernée")
    parser_ects.add_argument('--option', help="Option concernée")
    parser_ects.add_argument("--noms-suppl", nargs="+", help="Noms à sauter dans la liste", default=[])

    ## creer
    parser_creer = subparsers.add_parser("creer", parents=[common], help="Crée les tables pour un DS")
    parser_creer.add_argument('periode', help="Semestre S1 ou S2")
    parser_creer.add_argument('classes', nargs="+", help="Classes concernées")
    parser_creer.add_argument('--options', nargs="+", help="Options concernées")
    ## git
    parser_gitpull = subparsers.add_parser("gitpull", parents=[fonda], help="Fait un git pull sur le dépôt qui contient la base")
    parser_gitpush = subparsers.add_parser("gitpush", parents=[fonda], help="Commite le fichier de la base puis git push sur la branche courante")

    ## initialisation de la table etudiants
    parser_init = subparsers.add_parser("init", parents=[fonda], help="Initialise la base, crée la table des étudiants")

    args = parser.parse_args()
    tech.GLOBAL_debug = args.debug if hasattr(args, "debug") else False
    if not hasattr(args, "commande") or args.commande is None :
        parser.print_help()
    else :
        traiter(args)

if __name__ == "__main__" :
    main()

