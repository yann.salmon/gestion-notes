import subprocess
import time

from bdd import sans_accent

def recup_donnees(classe, bdd, semestre, option) :
    if option is None :
        les_ds = bdd.execute("select ds, coeff from classes_ds where coeff <> 0 and classe = ? and periode = ? and option is null", (classe, semestre)).fetchall()
    else :
        les_ds = bdd.execute("select ds, coeff from classes_ds where coeff <> 0 and classe = ? and periode = ? and option = ?", (classe, semestre, option)).fetchall()
    donnees = {}
    coeffs = {}
    somme_coeffs = 0.
    for l in les_ds :
        table_ds = l["ds"]
        if l["coeff"] is None or somme_coeffs is None:
            somme_coeffs = None
        else :
            coeffs[table_ds] = l["coeff"]
            somme_coeffs += l["coeff"]
        res_ds = bdd.execute("select id, nom, prenom, note, commentaire from " +table_ds + "_notes where classe=?", (classe,)).fetchall()
        for comm in res_ds:
            if comm["id"] not in donnees :
                donnees[comm["id"]] = {"nom" : comm["nom"], "prenom": comm["prenom"], "comm" : "", "notes" : {}}
            else :
                donnees[comm["id"]]["comm"] += "\n\n"
            if comm["commentaire"] is not None :
                donnees[comm["id"]]["comm"] += comm["commentaire"]
            donnees[comm["id"]]["notes"][table_ds] = comm["note"]
    if somme_coeffs is not None :
        for _, eleve in donnees.items() :
            someprod = 0.
            for table_ds in eleve["notes"] :
                if eleve["notes"][table_ds] is None :
                    someprod = None
                    break
                else :
                    someprod = someprod + eleve["notes"][table_ds] * coeffs[table_ds]
            if someprod is None :
                eleve["moyenne"] = None
            else :
                eleve["moyenne"] = someprod / somme_coeffs
    return donnees

def envoyer_element(n) :
    subprocess.call(["xdotool", "type", "--delay", "50", n +"\r"])


def envoyer_com(s, lmax=None) :
    s = s.replace("\n", " ").replace("\r", " ").replace("  ", " ").replace("~", " ").replace(r"\|", "").replace(r"\°", "").replace("|", "").replace("°", "")
    if lmax is not None :
        s = s[:lmax].strip()
    chaine = "".join(['echo "',s, '" | xsel -b'])
    subprocess.call(chaine, shell=True)
    subprocess.call(["xdotool", "key", "--delay", "100", "ctrl+v"])
    subprocess.call(["xdotool", "type",  "--delay", "50", "\t"])
    time.sleep(0.5)

def Pronote(args) :
    """Envoie une liste de notes à Pronote (placer le curseur dans la fenêtre de saisie du navigateur ; ça fonctionne en simulant des appuis au clavier)"""
    les_notes = args.bdd.execute("select nom, prenom, note from " + args.tbl_ds +"_notes where classe=?", (args.classe,)).fetchall()
    args.bdd.close()
    for x in args.noms_suppl :
        les_notes.append({"nom" : x, "prenom":"", "note": ""})
    les_notes.sort(key=lambda x : (sans_accent(x["nom"]), sans_accent(x["prenom"])))
    delay=5
    print(len(les_notes), "notes prêtes, vous avez", delay, "secondes")
    time.sleep(delay)
    for n in les_notes :
        if n["note"] is None :
            note = args.defaut
        elif n["note"] != "" :
            note = str(round(min(max(n["note"]*args.mult+args.add,0),20),2))
        else :
            note = n["note"]
        envoyer_element(note)

def valeur_ECTS(note) :
    seuils = [(3, "E"),
              (5,  "D"),
              (6, "C"),
              (7,  "B"),
              (9, "A")]
    if note is None :
        return "?"
    for s, v in reversed(seuils) :
        if note >= s :
            return v
    return " "


def ECTS(args) :
    donnees = [x[1] for x in recup_donnees(args.classe, args.bdd, args.periode, args.option).items()]
    for x in args.noms_suppl :
        donnees.append({"nom" : x, "prenom":"", "ignorer": True})
    donnees.sort(key=lambda x : (sans_accent(x["nom"]), sans_accent(x["prenom"])))
    lettres = [valeur_ECTS(x.get("moyenne")) for x in donnees]
    delay=5
    for k in range(len(donnees)) :
        print(donnees[k].get("nom"), donnees[k].get("prenom"), lettres[k])
    print(len(donnees), "mentions prêtes, vous avez", delay, "secondes")
    time.sleep(delay)
    for l in lettres :
        envoyer_com(l)

def Bulletin(args) :
    donnees = [x[1] for x in recup_donnees(args.classe, args.bdd, args.periode, args.option).items()]
    for x in args.noms_suppl :
        donnees.append({"nom" : x, "prenom":"", "ignorer": True})
    donnees.sort(key=lambda x : (sans_accent(x["nom"]), sans_accent(x["prenom"])))
    #donnees.sort(key = lambda x : sans_accent(x["nom"])

# def Bulletin(table_ds, classe, bdd) :
#     les_comm = bdd.execute("select nom, commentaire from " +table_ds + "_notes where classe=? order by nom collate ordonner_noms, prenom collate ordonner_noms", (classe,)).fetchall()
#     bdd.close()
    delay=5
    print(len(donnees), "commentaires prêts, vous avez", delay, "secondes")
    time.sleep(delay)
    for x in donnees :
        if x.get("ignorer", False) :
            envoyer_com("")
            continue
        envoyer_com(x["comm"])
#     for comm in les_comm :
#         envoyer_com(comm["commentaire"] if comm["commentaire"] is not None else "Étudiant absent au DS du semestre.", 600)
