## Technique
import subprocess
import tempfile
import os
import time
import io

def get_config() :
    global config
    config = {}
    try :
        with open(os.path.expanduser("~/.local/share/gestion-notes/config")) as f :
            for l in f :
                if l.startswith("#") :
                    continue
                cle, valeur = l.split("=", maxsplit=1)
                config[cle.strip()] = valeur.strip()
    except :
        pass

def get_desktop_dimensions() :
    bureaux = subprocess.check_output(["wmctrl", "-d"], universal_newlines=True)
    for b in bureaux.splitlines() :
        donnees = b.split()
        if donnees[1] == "*" :
            sresx, sresy = donnees[3].split("x")
            return int(sresx), int(sresy)

def graphical_editor(commandlist) :
    gedit = subprocess.Popen(commandlist)
    try :
        dx, dy = get_desktop_dimensions()
        width = 800
        height = 1000
        posx = (dx-width)//2
        posy = (dy-height)//2
        placement = ",".join([str(x) for x in [0, posx, posy, width, height]])
        wid = None
        pid = str(gedit.pid)
        debut = time.time()
        while wid is None and time.time() - debut < 2.0 :
            fenetres = subprocess.check_output(["wmctrl", "-ulp"], universal_newlines=True)
            for f in fenetres.splitlines() :
                donnees = f.split()
                if len(donnees) < 3 :
                    continue
                if donnees[2] == pid :
                    wid = donnees[0]
                    break
        if wid is not None :
            subprocess.call(["wmctrl", "-i", "-r", wid, "-e", placement])
        else :
            print("Impossible de trouver la fenêtre de l'éditeur de texte.")
    except Exception as e :
        print("Échec du placement de la fenêtre de l'éditeur de texte.")
        print(e)
    if gedit.wait() != 0 :
        raise ValueError("Le programme graphique a renvoyé un code d'erreur non nul.")


editorsPref = ["kate", "gedit", "nano"]

editorsCmd = {  "nano" : lambda n : subprocess.call(["nano", "--softwrap", "--nowrap", n]),
                "gedit" : lambda n : graphical_editor(["gedit", "-s", n]),
                "kate" : lambda n : graphical_editor(["kate", "--startanon", n])}

def editerTexte(ancienTexte) :
    f = tempfile.NamedTemporaryFile(mode='w+t', delete=False)
    n = f.name
    f.write(ancienTexte)
    f.close()
    success = False
    for editor in editorsPref :
        try :
            editorsCmd[editor](n)
            success = True
            break
        except Exception as e :
            if GLOBAL_debug : print(e)
            continue
    if success :
        with open(n) as f :
            texte = f.read().strip()
    else :
        texte = ancienTexte
        print("Aucun éditeur de texte n'a pu être lancé")
    os.remove(n)
    return texte


def tmpsymlink(src) :
    tmplink = tempfile.mktemp(os.path.basename(src))
    os.symlink(src, tmplink)
    return tmplink

def remove(fn) :
    global GLOBAL_debug
    if GLOBAL_debug :
        print("Fichier", fn, "non effacé (debug).")
    else :
        os.remove(fn)

def question(question, choix, sensible_casse = True, suppl_prompt=None) :
    reponse = ""
    if sensible_casse :
        prepr = lambda x : x
        rchoix = choix
    else :
        prepr = lambda x : x.tolower()
        rchoix = [x.tolower() for x in choix]
    prompt = question + " (" + "/".join(choix) + ")"
    if suppl_prompt is None :
        prompt += " "
    else :
        prompt += "\n" + suppl_prompt
    while not prepr(reponse) in rchoix :
        reponse = input(prompt)
    return reponse

def alterner(L) :
    """Alterne une liste pour la génération d'un document 2xA5 pratique."""
    res = []
    for k in range(0, len(L)//2) :
        res.append(L[k])
        res.append(L[k+(len(L)+1)//2])
    if len(L) % 2 == 1 :
        res.append(L[len(L)//2])
    return res

# création d'un fichier temporaire avec nom aléatoire (pour le .tex).
def MakeTempFile(pref, suff) :
    return tempfile.mkstemp(suffix=suff, prefix=pref, text=True)

def escape_latex(s) :
    return s.replace("_", r"\_")

def xdg_open(fn) :
    try:
        subprocess.Popen(["xdg-open", fn], stdin=None, stdout=None, stderr=None)
    except:
        print("L'ouverture du fichier a échoué, ouvrez-le manuellement depuis", fn)


class BaseLaTeX :
    
    def __init__(self) :
        raise NotImplementedError
    
    # Pour qu'on puisse faire print(..., file=ltx)
    def write(self, s) :
        self._f.write(s)
        
    def inclure_preambule(self, nf_preambule, defaut) :
        try :
            with open(nf_preambule) as fp :
                preambule = ["%%% Preambule inclus depuis " + nf_preambule + "\n"] + fp.readlines()
        except :
            preambule = ["%%% Preambule par defaut\n"] + defaut.splitlines()
        for l in preambule :
            print(l, end="", file=self._f)
        print(file=self._f)
        print(r"\begin{document}", file=self._f)
        
    def write_table(self, cles, donnees, reprNone = "---", align="l") :
        """Écrit dans le .tex des données tabulaires qui sont une liste de sqlite.Row ou une liste de dictionnaires. cles est la liste des clés (champs), doit être dans le même ordre que les "colonnes" des donnees"""
        f = self._f
        print(r"\noindent", file=f)
        print(r"\begin{longtable}["+align+ r"]{|" + "l|" * len(cles) + "}", file=f)
        print(r"\hline", file=f)
        try :
            print("&".join([r"\textbf{" + escape_latex(str(h)) + "}" for h in cles]) + r"\\\hline\endfirsthead", file=f)
            print(r"\hline", file=f)
            print(r"\multicolumn{", len(cles), r"}{|c|}{Suite de la page précédente}\\\hline", file=f)
            print("&".join([r"\textbf{" + escape_latex(str(h)) + "}" for h in cles]) + r"\\\hline\endhead", file=f)
            print(r"\multicolumn{", len(cles), r"}{|c|}{Suite sur la page suivante}\\\hline\endfoot", file=f)
            print(r"\endlastfoot", file=f)
            for ligne in donnees :
                print("&".join([reprNone if ligne[k] is None else escape_latex(str(ligne[k])) for k in cles]) + r"\\\hline", file=f)
        except Exception as e :
            print ("Erreur", escape_latex(str(e)), file=f)
        print(r"\end{longtable}", file=f)
    
    def write_histo(self, classes, l_donnees, titres = None, align="l", hauteur=5) :
        largeur = str(20/len(l_donnees))
        print(r"\leavevmode", file=self._f)
        if align != "l" :
            print(r"\hfill", file=self._f)
        print(r"""\begin{tikzpicture}
        \begin{axis}[width=18cm, height=""" + str(hauteur) + r"""cm, ybar , fill, hide y axis, axis x line*=bottom, x axis line style={draw opacity=0}, axis line shift=""" + str(-(10+(hauteur-5)*1.4)) + r"""pt, xtick=data, tick style={draw opacity=0 }, bar width="""+ largeur + r"""pt,nodes near coords, symbolic x coords = {""", file = self._f)
        print(",".join(classes), file=self._f)
        
        print(r"""},x tick label style={font=\scriptsize}, node near coord style={font=\scriptsize}]""", file=self._f)
        for kd in range(len(l_donnees)) :
            donnees = l_donnees[kd]
            print(r"""\addplot coordinates
            {""", file=self._f)
            for k in range(len(classes)) :
                print("("+ str(classes[k])+ ",", donnees[k], ")", file=self._f)
            print(r"""};""", file=self._f)
            if titres is not None :
                print(r"""\addlegendentry{""", titres[kd], r"""}""", file=self._f)
        print(r"""
        \end{axis}
        \end{tikzpicture}""", file=self._f)
        if align == "c" :
            print(r"\hfill\null", file=self._f)
            
    def write_sql(self, bdd, req, param=None, reprNone = "---", entete = False, align="l") :
        """Écrit dans le .tex la réponse à une requête sql SELECT"""
        f = self._f
        try :
            if param is None :
                cur = bdd.execute(req)
            else :
                cur = bdd.execute(req, param)
            cles = bdd.getKeys(cur)
            resultat = cur.fetchall()
        except Exception as e :
            print ("Erreur", str(e), file=f)
            return
        if entete :
            print(r"\noindent Résultat de la requête SQL~:", file=f)
            print(r"\begin{lstsql}", file=f)
            print(req, file=f)
            print(r"\end{lstsql}", file=f)
            print(r"Nombre de lignes~:", len(resultat), r"\\", file=f)
        self.write_table(cles, resultat, reprNone, align)
        
    def finaliser(self) :
        """Termine le .tex, le ferme"""
        print(r"""\end{document}""", file=self._f)
        self._f.close()

class MemLaTeX(BaseLaTeX) :
    def __init__(self) :
        self._f = io.StringIO()
        
    def getvalue(self) :
        return self._f.getvalue()
        
class FileLaTeX(BaseLaTeX) :
        
    def __init__(self, fn) :
        """Crée un nouveau .tex temporaire et écrit son préambule"""
        os.makedirs(os.path.join("/tmp", os.path.dirname(fn)), exist_ok=True)
        self._fn = os.path.join("/tmp", fn + ".tex")
        self._f = open(self._fn, "w")
    
    def filename(self) :
        return self._fn
            
    def _runLaTeX(self, wait=True) :
        if wait :
            subprocess.call(["latexmk", "-pdfxe", "--silent", "-bibtex-cond", "-cd", "-interaction=batchmode",  "-halt-on-error",  self._fn])
            if not GLOBAL_debug :
                subprocess.call(["latexmk", "-cd", "-c", self._fn])
                remove(self._fn)
        else :
            subprocess.Popen(" ".join(["latexmk", "-pdfxe", "--silent", "-bibtex-cond", "-cd", self._fn] + ([] if GLOBAL_debug else ["&& latexmk", "-cd", "-c", self._fn, "&& rm", self._fn])), shell=True, executable="/bin/bash", stdin=None, stdout=open(os.devnull, "wb"), stderr=None)

    def _showPDF(self) :
        xdg_open(self._fn.replace(".tex", ".pdf"))
