import subprocess
import os
import shutil
import csv
import tech
from moteur import DS


class SDAPS :
    def __init__(self, basename, basedir, quest_raw) :
        self._dir = os.path.join(basedir, basename+"_qcm")
        self._lquestions = quest_raw
    
    def dossier_existe(self) :
        return os.path.exists(self._dir)
        
    def effacer_dossier(self) :
        shutil.rmtree(self._dir)

    def gerer_dossier(self, autodelete) :
        if self.dossier_existe() :
            if autodelete :
                print("Le dossier " + self._dir + " existe déjà. On l'efface.")
                deldir = True
            else :
                effacer = tech.question("Le dossier " + self._dir + " existe déjà. L'effacer ?", ["o", "n"])
                deldir = effacer == "o"
            if deldir :
                self.effacer_dossier()
            else :
                raise Exception("Abandon")
        os.makedirs(self._dir)
    
    def nbquest(self) :
        return len(self._lquestions)
    
    def nmaxrep(self) :
        return max([x["nbrep"] for x in  self._lquestions])
    
    def nbrep(self, nq) :
        return self._lquestions[nq]["nbrep"]
        
    def repjuste(self, nq, nr) :
        return self._lquestions[nq]["corrige"][nr]
        
class SDAPSRead(SDAPS) :
    def __init__(self, basename, basedir, lquestions) :
        super().__init__(basename, basedir, lquestions)
        if not self.dossier_existe() :
            raise ValueError("Le dossier " + self._dir + " n'existe pas.")
        self.__csvfn = os.path.join(self._dir, "resultats.csv")

    
    def importer_fichiers(self, lnf, do_convert) :
        subprocess.call(["sdaps", "add", self._dir] + lnf + (["--convert"] if do_convert else []))
    
    def resultats(self) :
        subprocess.call(["sdaps", "recognize", self._dir])
        subprocess.call(["sdaps", "csv", "export", self._dir,  "-o", self.__csvfn])
        f = open(self.__csvfn)
        dialect = csv.Sniffer().sniff(f.readline(), delimiters=",;")
        f.seek(0)
        csvr = csv.DictReader(f, dialect=dialect)
        res = []
        nbquest = self.nbquest()
        try :
            while True :
                copie = next(csvr)
                id_etu = int(copie["questionnaire_id"])
                res_etu = [[] for q in range(nbquest)]
                for q in range(0, nbquest) :
                    for r in range(0, self.nbrep(q)) :
                        if copie["1_"+str(q+1)+"_"+str(r+1)] == "1" :
                            res_etu[q].append(chr(65+r))
                    res_etu[q].sort()
                res.append({"id" : id_etu, "rep" : res_etu})
        except StopIteration :
            pass
        f.close()
        tech.remove(self.__csvfn)
        return res
        
class CSVGen(SDAPS) :
    def __init__(self, basename, basedir, lquestions, autodelete, Ax="4") :
        super().__init__(basename, basedir, lquestions)
        self.gerer_dossier(autodelete)

    def generer(self, ldict) :
        recap = open(os.path.join(self._dir, "mailmerge.csv"), "w")
        gcsv = csv.DictWriter(recap, fieldnames=tuple(ldict[0].keys())+("PJ","nomse"))
        gcsv.writeheader()
        for eleve in ldict :
            nf = os.path.join(self._dir, "{}_{}_{}_{}.csv".format(eleve["id"], eleve["classe"], eleve["nom"].replace(" ", ""), eleve["prenom"].replace(" ", "")))
            f = open(nf, "w")
            csvw = csv.writer(f)
            for q in range(self.nbquest()) :
                csvw.writerow([eleve["id"], q+1])
            f.close()
            gcsv.writerow({**eleve, "PJ":nf, "nomse":eleve["nom"].replace(" ", "")})
        recap.close()


class SDAPSGen(SDAPS) :
    def __init__(self, basename, basedir, lquestions, autodelete, Ax="4") :
        super().__init__(basename, basedir, lquestions)
        self.__outfile = os.path.join(self._dir, basename + "_qcmas.pdf")
        if Ax.endswith("m") :
            self._Ax = Ax[:-1]
            self._gerermulti = True
        else :
            self._Ax = Ax
            self._gerermulti = False
        self.gerer_dossier(autodelete)
        self.__ltx = tech.FileLaTeX(basename+"_qcm")
        print(r"""\documentclass[
  a""" + str(self._Ax) + r"""paper,
  french,
  pagemark,
  stamp,
  10pt,
  oneside,
  print_questionnaire_id,
  no_print_survey_id,
  checkmode=fill
]{sdapsclassic}
\ExplSyntaxOn
\sdaps_context_set:n { * = { ellipse } }
\ExplSyntaxOff
\title{"""+tech.escape_latex(basename)+"}", file=self.__ltx)
        (fd, self.__idsfn) = tech.MakeTempFile(basename+"_qcm", ".ids")
        self.__idsf = open(fd, "w")

    
    def inclure_preambule(self, nf_preambule, default) :
        self.__ltx.inclure_preambule(nf_preambule, default)
    
    def enregistrer_noms_id(self, ldict) :
        les_cles = ["nom", "prenom", "classe"]
        for cle in  les_cles :
            print(r"\pgfkeyssetvalue{/sdaps/qid//"+cle+"}{*" + cle + "*}", file=self.__ltx)
        if self._gerermulti :
            ldict = tech.alterner(ldict)
        for eleve in ldict :
            for cle in les_cles :
                print(r"\pgfkeyssetvalue{/sdaps/qid/"+str(eleve["id"])+"/"+cle+"}{" + eleve[cle] + "}", file=self.__ltx)
            print(eleve["id"], file=self.__idsf)
            
    def formulaire(self) :
        nmaxrep = self.nmaxrep()
        print(r"""\begin{questionnaire}[noinfo]
        \setcounter{section}{0}
        \setcounter{subsection}{0}
        \centerline{\LARGE \pgfkeys{/sdaps/qid/\qid/nom}}
        \centerline{\Large \pgfkeys{/sdaps/qid/\qid/prenom}}
        \centerline{\small \pgfkeys{/sdaps/qid/\qid/classe}}""", file=self.__ltx)
        
        print(r"\begin{info}\scriptsize\centering Pour cocher une case, remplissez-la entièrement \textbf{en noir}.\\Il n'est pas possible de décocher une case, aussi réfléchissez avant de répondre.\\Ne pliez pas, ne froissez pas, ne salissez pas la feuille~; ça peut rendre sa lecture impossible.\end{info}\par\vskip\parskip", file=self.__ltx)
                
        print(r"\begin{choicegroup}[multichoice]{}", file=self.__ltx)
        for k in range(0, nmaxrep) :
            print(r"\groupaddchoice{",chr(65+k),"}", sep="", file=self.__ltx)
        for k in range(self.nbquest()) :
            print(r"\question[range={...,",self.nbrep(k),"}]{QCM ",k+1,"}", sep="", file=self.__ltx)
        print(r"\end{choicegroup}", file=self.__ltx)
        
        print(r"\end{questionnaire}", file=self.__ltx)
        
    def finaliser(self) :
        self.__ltx.finaliser()
        self.__idsf.close()
        subprocess.call(["sdaps", "setup", self._dir, self.__ltx.filename(), "-e", "xelatex"])
        subprocess.call(["sdaps", "ids", self._dir, "--add", self.__idsfn])
        subprocess.call(["sdaps", "stamp", self._dir, "--existing", "-o", self.__outfile])
        tech.xdg_open(tech.tmpsymlink(self.__outfile))
        tech.remove(self.__idsfn)
        tech.remove(self.__ltx.filename())
        
        
def GenererQCM(args) :
    ds = DS(args)
    # à ce stade, on a une vue temporaire tv_etu qui contient les noms des étudiants
    les_etus = ds.select_from_etu("*", "ORDER BY classe, nom").fetchall()
    quest_raw = ds.select_from_qcm("*", "ORDER BY quest").fetchall()
    if args.Ax == "csv" :
        cg = CSVGen(args.tbl_ds, args.basedir, quest_raw, args.autodelete, Ax)
        cg.generer(les_etus)
    else :
        sdaps = SDAPSGen(args.nom_ds, args.basedir, quest_raw, args.deldir, args.Ax)
        sdaps.enregistrer_noms_id(les_etus)
        sdaps.inclure_preambule(args.preambule, "")
        sdaps.formulaire()
        sdaps.finaliser()
    
def ImporterReponsesQCM(args) :
    ds = DS(args)
    quest_raw = ds.select_from_qcm("*", "ORDER BY quest").fetchall()
    sdaps = SDAPSRead(args.tbl_ds, args.basedir, quest_raw)
    sdaps.importer_fichiers(args.fichiers, args.convert)
    copies = sdaps.resultats()
    for copie in copies :
        ds.EnregistrerRepQCM(copie["id"],  copie["rep"], True) 

def lettres2tableau(chaine, nbrep) :
    res = [False for k in range(nbrep)]
    if chaine is None :
        return res
    chaine = chaine.strip().upper()
    for c in chaine :
        cc = ord(c)-65
        if cc < 0 or cc > len(res) :
            print("Valeur interdite en QCM", c)
            continue
        res[ord(c)-65] = True
    return res

def NoterQCM(args) :
    ds = DS(args)
    if not ds.AQCM() :
        return
    questions = ds.select_from_qcm("*", "ORDER BY quest").fetchall()
    for k in range(len(questions)) :
        questions[k] = dict(questions[k])
        questions[k]["nb_bonnesrep"] = len(questions[k]["corrige"])
        questions[k]["corrige"] = lettres2tableau(questions[k]["corrige"], questions[k]["nbrep"])
    reponses = ds.LireToutesRepQCM()
    for rep in reponses :
        quest = questions[rep["quest"]-1]
        if rep["reponse"] is None or rep["reponse"] == "" or quest["points_vrai"] is None or quest["points_faux"] is None or quest["coeff"] is None :
            points = None
        else :
            reptab = lettres2tableau(rep["reponse"], quest["nbrep"])
            points = 0
            for r in range(len(reptab)) :
                if reptab[r] :
                    if quest["corrige"][r] :
                        points += quest["points_vrai"]
                    else :
                        points -= quest["points_faux"]
            points = points * quest["coeff"] / (quest["nb_bonnesrep"]*quest["points_vrai"])
        ds.EnregistrerPointsQCM(rep["id"], rep["quest"], points)
    ds.EnregistrerTotalPointsQCM()
