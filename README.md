# Présentation

Ce script python vise à gérer des corrections de DS : calcul des notes sur 20, génération de fiches de résultats. Cela permet de ne plus écrire de notes sur les copies (ni globale, ni par question) mais seulement des annotations et remarques.

# Utilisation de base

Le script python s'utilise depuis un terminal.
## Initialisation

Les données sont dans une base sqlite qui est idéalement créée (avec les tables idoines) par ```ds init```.

Par défaut, le script utilise une base nommée ```base.sqlite``` située ```~/Documents/gestion-notes``` (ou le dossier qu'indique ```xdg-user-dir DOCUMENTS```) et considère que la table des étudiants s'appelle ```etudiants```. Toutes les commandes du script permettent de spécifier une autre base et un autre nom pour cette table.

Il y a aussi une table ```classes_ds``` qui permet d'associer à chaque DS des classes (éventuellement une option) et une période (voir plus bas).

Il est possible de changer le nom et l'emplacement de la base par défaut en créant un fichier ```~/.local/share/gestion-notes/config``` et en y écrivant ```defaultBase=…```.

On peut obteni la liste des devoirs existants avec ```ds listeds```.

## Création d'un DS
La commande
```
ds creer machin periode classe1 classe2 [--option opt]
```

crée les tables nécessaires à un ds de nom machin,
soit ```machin_struct```, ```machin_copie``` et ```machin_result```. La période (S1 ou S2, par exemple) sert pour le calcul éventuel des moyennes et des commentaires du bulletin. La liste des classes permet de déterminer quels sont les élèves concernés par ce DS, idem concernant l'éventuelle option.

```machin_struct``` contient la structure du ds : pour chaque item, le nombre de
points de barème et le poids dans la note sur 20 (le script fera une règle de
trois).
Cette table doit être remplie à la main, à l'aide d'un éditeur de table par
exemple (sqliteman, etc.).

```machin_result``` sera remplie par le script au fur et à mesure de la correction.

## Correction
On démarre une session de correction interactive pour le DS machin avec ```ds corriger machin```.

Si on a un ordinateur mais pas d'accès au script, on peut enregistrer sa saisie dans un fichier texte simple. Les différences avec la syntaxe de la session interactive sont :
  * la fin de chaque copie doit être signalée par ```---```, même si la dernière question a été atteinte
  * le commentaire de la copie est donné en commençant une unique ligne par ```*```

On importe ces saisies par ```ds corriger machin --batch nom_fichier```.

On peut faire une correction interactive question par question au lieu de copie par copie en ajoutant ```--serie``` sur la ligne de commande.

## Génération de rapports et fiches de notes

Ces fonctions requièrent une installation LaTeX (latexmk et xelatex).

```
ds rapport machin
```
génère un rapport statistique (classements, etc.)

```
ds fiches machin
```
génère un PDF de résultat pour chaque élève

## Exports vers Pronote
Pronote ne proposant pas d'API, on simule des appuis clavier dans une fenêtre ouverte manuellement. Cela requiert ```xdotool``` et met la pagaille dans le presse-papier.

```
ds pronote machin classe [--option opt]
```
envoie les notes de la classe indiquée à Pronote. Des options sont disponibles pour régler ce qui est envoyé en l'absence de note et aussi pour ajouter des facteurs multiplicatif et additif à toutes les notes lors de l'envoi.

```
ds bulletin periode classe [--option opt]
```
envoie à Pronote comme commentaire de bulletin le commentaire de chaque élève. Ce commentaire est la concaténation des commentaires des copies des DS de la période.


# Choix de la base de données et du répertoire de travail

Il est toujours possible de spécifier la base de données à utiliser avec ```--base /chemin/de/la/base.sqlite```.

Si cette option n'est pas présente, le script utilise un fichier ```base.sqlite``` situé dans le répertoire où se situe le script invoqué (sans déréférencer un éventuel lien symbolique vers le script).

Dans les deux cas, le répertoire de travail sera le répertoire contenant réellement le fichier de la base de données (cette fois, en déréférençant un éventuel lien symbolique).

Les rapports et les fichiers temporaires sont placés dans le répertoire temporaire du système.

# Utilisation mobile
Le script devrait fonctionner sur un téléphone Android avec l'application [Termux](https://f-droid.org/packages/com.termux/) dès lors que les dépendances sont installées.

# QCM

Le module de gestion des QCM repose sur [sdaps 1.9 ou ultérieur](https://sdaps.org/).

Le processus consiste à générer pour chaque élève une fiche-réponse à son nom qui ne contient pas l'intitulé des questions. Cela permet de distribuer ces fiches avant le DS sans en dévoiler le contenu. Chaque fiche porte un QR code qui peut être relu par SDAPS, ce qui permet d'associer chaque fiche scannée à l'élève concerné.
## Initialisation
Un QCM est nécessairement associé à un DS, qui doit donc être préalablement créé.
On crée les tables nécessaires à la gestion du QCM par ```ds creerqcm nom_ds```. Il s'agit de deux tables : une dont le nom se termine par ```_qcm``` et qui contient la structure du QCM, l'autre dont le nom se termine par ```_qcmrep``` et qui contiendra les réponses lues depuis SDAPS.
## Définition des questions
On définit la structure du QCM en remplissant manuellement la table ```_qcm```.

Le champ ```quest``` correspond au numéro de la question ; les questions doivent avoir des numéros consécutifs commençant à 1 (mais elles peuvent être dans le désordre dans la table).

Le champ ```nbrep``` contient un entier qui est le nombre de réponses proposées pour cette question.

Le champ ```corrige``` contient la liste des réponses correctes sous la forme d'une succession de lettres (par exemple ```BD``` si les réponses correctes sont la deuxième et la quatrième).

Le champ ```points_vrai``` est un entier qui donne la valorisation pour chaque réponse cochée qui est correcte. Le champ ```points_faux``` est un entier _positif_ qui donne la dépréciation pour chaque réponse cochée qui est incorrecte. Aucun point n'est enlevé (ni, évidemment, ajouté) pour une réponse non cochée alors qu'elle est correcte.

Le champ ```coeff``` donne le poids de la question : il s'agit du nombre de points qui est marqué lorsqu'on donne toutes les bonnes réponses et seulement celles-là. Lors de la génération du rapport ou des fiches, les ```coeff``` des questions sont additionnés pour former le ```bareme``` de la question ```qcm```.

## Génération des fiches réponses
Lorsque la structure du QCM est définie, il est possible de générer un fichier PDF comprenant une fiche réponses pour chaque élève concerné par le DS auquel se rattache le QCM.

On effectue tout simplement ```ds genqcm nom_ds```. On peut ajouter ```--ax 5``` pour générer des fiches au format A5 (fonctionne de A6 à A3). Avec le mode ```5m``` (ou ```4m```, etc.), on active une réorganisation des pages qui rendra plus facile leur séparation si elles sont imprimées deux par deux (deux pages A5 sur une feuille A4). Le format par défaut est A4 mais on peut le changer dans le fichier de configuration ```~/.local/share/gestion-notes/config``` avec la clé ```defaultPDFpaper```.

Au moment de la génération, un projet SDAPS est créé dans un sous-dossier du dossier contenant la base de données (on peut changer avec ```--basedir```). Ces informations ne doivent pas être altérées sinon SDAPS ne pourra pas analyser les fiches réponses scannées.

## Importation des réponses
On effectue ```ds repqcm nom_ds fichier_scanné.pdf```. Les options de conversion automatique de SDAPS sont utilisées si on a utilisé ```--convert```, mais elles ne fonctionnent que si les logiciels nécessaires sont installés. Consulter la documentation de SDAPS.

Les réponses sont enregistrées dans la table ```_qcmrep```.

## Notation du QCM et intégration au résultat du DS
Pour le DS, le QCM est vu comme une unique question, nommée ```qcm```, dont le nombre total de points de barème est le nombre de points qu'on obtient en cochant toutes les bonnes réponses et seulement celles-là.

Pour chaque élève, les réponses analysées par SDAPS sont comparées au corrigé, la table ```_qcmrep``` est mise à jour avec la note de chaque question à choix multiples et le total est porté dans la table ```_result``` dans la question ```qcm```.

Cette opération est effectuée à chaque fois qu'un rapport ou des fiches de notes sont générées, mais seulement dans ces cas là. Cela signifie que si on change la structure du QCM, la notation, et donc la note sur 20 de la vue ```_notes``` devient invalide jusqu'à la prochaine demande de rapport ou fiches.
