from moteur import PlusieursEleves, AucunEleve, NoteExistante, MauvaisNbItems, ItemTropGrand
from tech import question, editerTexte

from interf_interact import *

import tempfile
import subprocess
import os



class InterfaceClavier(InterfaceInteract) :

    def __init__(self, args) :
        super().__init__(args)
        self.prompt_commentaire = not args.no_comment
        self.q_debut = 0
        if args.debut is not None :
            try :
                self.q_debut = self.ds.QuestionNommee(args.debut, None)
            except ValueError :
                print("Question de début {} introuvable", args.debut)
                exit(1)
        self.q_fin = self.ds.NombreQuestions()
        if args.fin is not None :
            try :
                self.q_fin = self.ds.QuestionNommee(args.fin, None) + 1
            except ValueError :
                print("Question de fin {} introuvable", args.debut)
                exit(1)


    def Corriger(self) :
        try :
            while True :
                self.TraiterCopie()
        except SortieEleve :
            self.NbCopies()
            self.ds.commit()
            self.ds.close()


    def TraiterQuestion(self, eleve, q_nb) :
        """Gère le processus de saisie d'une note pour une question, c'est-à-dire qu'on signale les saisies incorrectes détectées par les fonctions auxiliaires, qu'on vérifie que la question demandée existen qu'on a rentré le bon nombre d'items, que les notes sont possibles et tente l'inscription effective de cette note.
        Renvoie le fait qu'on a réussi ce qu'on voulait, ce qui permet à la fonction appelante de savoir si elle doit passer à la question suivante ou rester sur celle-ci."""
        try :
            (replace, repq, note) = self.InputNote(eleve, q_nb)
        except ValueError :
            print("Erreur de saisie")
            return q_nb, False
        except SaisieCommentaire :
            self.InputCommentaire(eleve["id"], True)
            return q_nb, False
        except SaisieAide :
            self.InputAide(q_nb)
            return q_nb, False
        if repq is None :
            question = self.ds.NomQuestion(q_nb)
        else :
            try :
                q_nb = self.ds.QuestionNommee(repq, q_nb)
                question = self.ds.NomQuestion(q_nb)
            except ValueError :
                print("Question "+ repq + " introuvable")
                return q_nb, False
        if note is None :
            return q_nb, repq is None
        if note == ActionNote.Delete :
            try :
                print("Effacement de la note pour la question", question)
                self.ds.EffacerNote(eleve["id"], q_nb)
                return q_nb, True
            except :
                print("Impossible d'effacer la note pour la question", question)
                return q_nb, False
        try :
            return q_nb, self.InsereNote(eleve["id"], q_nb, note, replace)
        except MauvaisNbItems :
            print ("Mauvais nombre d'items pour la question " + question)
            return q_nb, False
        except ItemTropGrand :
            print ("Valeur trop élevée pour un item en question "+ question)
            return q_nb, False


    def TraiterCopie(self) :
        """Traite une copie en demandant d'abord à qui elle est, puis en itérant dans les questions du sujet"""
        try :
            eleve = self.InputEleve()
        except ValueError :
            print("Erreur de saisie")
            return
        print("Copie de ", list(eleve))
        q_nb = self.q_debut
        try :
            while q_nb < self.q_fin :
                (q_traitee, reussi) = self.TraiterQuestion(eleve, q_nb)
                if reussi :
                    q_nb = q_traitee + 1
                else :
                    q_nb = q_traitee
            print("Copie terminée")
        except SortieQuestion :
            pass
        self.NoteTotale(eleve)
        if self.prompt_commentaire :
            self.InputCommentaire(eleve["id"])

