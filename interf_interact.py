from moteur import PlusieursEleves, AucunEleve, NoteExistante, MauvaisNbItems, ItemTropGrand
from tech import question, editerTexte

from interf_texte import *

import tempfile
import subprocess
import os



class InterfaceInteract(InterfaceTexte) :

    def __init__(self, args) :
        super().__init__(args)


    def DesambiguerEleve(self, l_eleves) :
        """Permet de choisir un élève parmi une liste."""
        reponse = -1
        print("Plusieurs choix possibles")
        for k in range(len(l_eleves)) :
            print(k+1, " : ", list(l_eleves[k]))
        while reponse not in range(len(l_eleves)+1) :
            try :
                reponse = int(input("Quel élève (0 pour revenir) ? "))
            except :
                reponse = -1
        if reponse == 0 :
            return None
        return l_eleves[reponse - 1]

    def InputEleve(self) :
        """Demande le nom d'un élève en vue de sa sélection."""
        eleve = None
        while eleve is None :
            nom = input("Entrez le nom d'un élève (---- pour sortir) ")
            if nom == "----" :
                raise SortieEleve
            if " " in nom :
                (nom, prenom) = nom.split(" ")
            else :
                prenom = None
            try :
                eleve = self.ds.SelectionnerEleve(nom, prenom)
            except AucunEleve :
                print("Aucun résultat")
            except PlusieursEleves as e :
                eleve = self.DesambiguerEleve(e.liste)
        return eleve



    def InputNote(self, eleve, q_nb) :
        """Réalise l'interface pour la fonction ParseNote. On ne fait que de l'affichage et des vérifications formelles."""
        les_items = self.ds.ItemsQuestion(q_nb)
        prompt = self.ds.NomQuestion(q_nb) + " ("
        prompt += "/".join([str(item[0]) + " sur " + str(item[1]) for item in les_items])
        prompt += ") ("
        prompt += self.ds.AideQuestion(q_nb)
        prompt += ") "
        ancien = self.ds.AncienneNote(eleve, q_nb)
        if ancien is not None :
            prompt += "(ancien : " + ancien + ") "
        return self.ParseNote(input(prompt))



    def InsereNote(self, id, q_nb, note, replace) :
        """Effectue l'inscription d'une note dans la BDD. En cas de conflit (note déjà existante), demande confirmation si replace est False.
        Renvoie le fait qu'on a réussi l'inscription."""
        try :
            succes, repl  = self.ds.EnregistrerNote(id, q_nb, note, replace)
            if repl :
                print("Remplacement de la note en question", self.ds.NomQuestion(q_nb))
            return succes
        except NoteExistante :
            reponse = question("Erreur d'intégrité, voulez-vous remplacer la note existante ?", ["o", "n"])
            if reponse == "o" :
                return self.InsereNote(id, q_nb, note, True)
            else :
                return False


    def InputCommentaire(self, eleve_id, noconf = False)  :
        anciencom = self.ds.AncienCommentaire(eleve_id)
        if anciencom is None :
            anciencom = ""
        if noconf or question("Commentaire actuel : " + anciencom + "\nÉditer commentaire ?", ["o", "n"]) == "o" :
            self.ds.EnregistrerCommentaire(eleve_id, editerTexte(anciencom))

    def InputAide(self, q_nb) :
        self.ds.EnregistrerAide(q_nb, editerTexte(self.ds.AideQuestion(q_nb)))


