import enum


class ErreurEleve(Exception) :
    pass

class PlusieursEleves(ErreurEleve) :
    def __init__(self, liste) :
        self.liste = liste

class AucunEleve(ErreurEleve) :
    pass
    
class ErreurNote(Exception) :
    pass

class NoteExistante(ErreurNote) :
    pass

class MauvaisNbItems(ErreurNote) :
    pass
    
class ItemTropGrand(ErreurNote) :
    pass
    

        
def ItemsOK(les_items, note) :
    """Vérifie qu'on a bien donné des notes inférieures au barème"""
    for k in range(len(les_items)) :
        if note[k] is not None and note[k] > les_items[k][1] :
            return False
    return True

def genNotes(id, question, les_items, note) :
    for k in range(len(les_items)) :
        if note[k] is not None :
            yield (id, question, les_items[k][0], note[k])


## Initialisation d'un DS

def Initialiser(args) :
    args.bdd.execute("""CREATE TABLE IF NOT EXISTS """+args.tbl_etu+""" ( "id" "INTEGER" PRIMARY KEY AUTOINCREMENT,
"nom" "TEXT" NOT NULL,
"prenom" "TEXT" NOT NULL,
"genre" "TEXT" NOT NULL,
"classe" "TEXT" NOT NULL,
"option" "TEXT",
"groupe" "TEXT",
"email" "TEXT"
)""")
    args.bdd.execute("CREATE TABLE IF NOT EXISTS classes_ds (ds TEXT NOT NULL, classe TEXT NOT NULL, option TEXT, periode TEXT NOT NULL, coeff REAL)")
    args.bdd.commit()
    args.bdd.close()

def CreerQCM(args) :
    """Ajoute une table pour gérer un qcm"""
    args.bdd.execute("CREATE TABLE " + args.nom_ds + "_qcm (quest int not null, nbrep int not null, corrige text, points_vrai int, points_faux int, coeff int, PRIMARY KEY(quest))")
    args.bdd.execute("CREATE TABLE " + args.nom_ds + "_qcmrep (id int not null, quest int not null, reponse text not null, points int, PRIMARY KEY(id, quest))")

def CreerDS(args) :
    """Ajoute deux tables et une vue dans la bdd pour un DS nommé nom_ds. Une table pour la structure du DS, une table pour les résultats des élèves question par question, une vue pour les notes"""
    # table de métadonnées
    bdd, nom_ds, nom_ds_etu, classes, options, periode = args.bdd, args.tbl_ds, args.tbl_etu, args.classes, args.options, args.periode
    
    if options is None or len(options) == 0 :
        for classe in classes :
            bdd.execute("INSERT INTO classes_ds VALUES (?, ?, ?, ?, null);", (nom_ds, classe, None, periode))
    else :
        for classe in classes :
            for option in options :
                bdd.execute("INSERT INTO classes_ds VALUES (?, ?, ?, ?, null);", (nom_ds, classe, option, periode))
    
    
    
    # tables du DS
    bdd.execute("CREATE TABLE IF NOT EXISTS "+ nom_ds+ "_struct (question text NOT NULL, item text NOT NULL, bareme int NOT NULL, points real, PRIMARY KEY(question, item))")
    bdd.execute("INSERT OR IGNORE INTO "+nom_ds+"_struct VALUES ('penalite', '', 8, -4)")
    bdd.execute("CREATE TABLE IF NOT EXISTS "+ nom_ds+ "_aide (question text NOT NULL, aide text, PRIMARY KEY(question))")
    bdd.execute("CREATE TABLE IF NOT EXISTS "+ nom_ds+ "_result (question text NOT NULL, item text NOT NULL, id int NOT NULL, points int, commentaire TEXT, PRIMARY KEY(question, item, id))")
    bdd.execute("CREATE TABLE IF NOT EXISTS "+ nom_ds + "_copie (id int NOT_NULL, commentaire TEXT, PRIMARY KEY(id))")
    les_classes = "("+",".join(["'"+x+"'" for x in classes])+")"
    if options is None or len(options) == 0 :
        critere_option = ""
    else:
        critere_option = " AND option IN (" + ",".join(["'"+x+"'" for x in options]) +")"
    bdd.execute("CREATE VIEW IF NOT EXISTS "+nom_ds+"_notes AS SELECT tbl_etu.*, ROUND(SUM("+nom_ds+"_result.points * "+nom_ds+"_struct.points / "+nom_ds+"_struct.bareme),2) AS note, "+nom_ds+"_copie.commentaire AS commentaire FROM ((SELECT * FROM "+nom_ds_etu+" WHERE classe IN " + les_classes + critere_option+ ") AS tbl_etu LEFT JOIN ("+nom_ds+"_result JOIN "+nom_ds+"_struct USING (question, item)) ON tbl_etu.id = "+nom_ds+"_result.id) LEFT JOIN "+nom_ds+"_copie ON tbl_etu.id="+nom_ds+"_copie.id GROUP BY tbl_etu.id")
    bdd.commit()
    bdd.close()
    
def ListeDS(args) :
    data = args.bdd.execute("SELECT * FROM classes_ds;").fetchall()
    infos = {}
    for x in data :
        ds = x["ds"]
        if ds not in infos :
            infos[ds] = ( [x["classe"] for x in data if x["ds"]==ds],
                    x["option"],
                    x["periode"])
    for ds in infos :
        print(ds, infos[ds])
    
class DS_base :
    def __init__(self, args) :
        self.bdd = args.bdd
        self.table_ds = args.tbl_ds
        self.table_result = args.tbl_ds + "_result"
        self.table_struct = args.tbl_ds + "_struct"
        self.table_aide = args.tbl_ds + "_aide"
        self.table_notes = args.tbl_ds + "_notes"
        self.table_copie = args.tbl_ds + "_copie"
        self.table_qcm = args.tbl_ds + "_qcm"
        self.table_qcmrep = args.tbl_ds + "_qcmrep"

        meta = self.bdd.execute("SELECT classe, option, periode FROM classes_ds WHERE ds = ?", (self.table_ds,)).fetchall()
        self.classes = tuple(x["classe"] for x in meta)
        self.options = tuple(x["option"] for x in meta if x["option"] is not None)
        self.periode = meta[0]["periode"]
                

    def select_from(self, table, champs, complement, *etc) :
        return self.bdd.execute("SELECT " + champs + " FROM " + table + " " + complement, *etc)

    def select_from_struct(self, *etc) :
        return self.select_from(self.table_struct, *etc)
        
    def select_from_aide(self, *etc) :
        return self.select_from(self.table_aide, *etc)
        
    def select_from_result(self, *etc) :
        return self.select_from(self.table_result, *etc)

    def select_from_notes(self, *etc) :
        return self.select_from(self.table_notes, *etc)
        
    def select_from_qcm(self, *etc) :
        return self.select_from(self.table_qcm, *etc)
        
    def close(self) :
        return self.bdd.close()

    def CopiesManquantes(self) :
        id_repondus = self.select_from_result("DISTINCT id", "WHERE question <> ?", ("qcm",)).fetchall()
        return self.select_from_notes("*", "WHERE id NOT IN (" + ",".join(["?" for _ in id_repondus]) + ")", tuple(x[0] for x in id_repondus)).fetchall()
        
    def QCMManquants(self) :
        try :
            id_repondus = self.select_from(self.table_qcmrep, "DISTINCT id", "").fetchall()
        except self.bdd.OperationalError :
            return None
        return self.select_from_notes("*", "WHERE id NOT IN (" + ",".join(["?" for _ in id_repondus]) + ")", tuple(x[0] for x in id_repondus)).fetchall()
        
    def CopiesQCMManquants(self) :
        return self.CopiesManquantes(), self.QCMManquants()

class DS(DS_base) :
    def __init__(self, args) :
        super().__init__(args)
        self.commit_note = True
        if len(self.options) == 0 :
            self.bdd.execute("CREATE TEMPORARY VIEW tv_etu AS SELECT * FROM " + args.tbl_etu + " WHERE classe in (" + ",".join(["'" + x + "'" for x in self.classes]) + ")")
        else :
            self.bdd.execute("CREATE TEMPORARY VIEW tv_etu AS SELECT * FROM " + args.tbl_etu + " WHERE classe in (" + ",".join(["'" + x + "'" for x in self.classes]) + ") AND option in (" + ",".join(["'" + x + "'" for x in self.options]) + ")")
        self.bdd.commit()
        self.table_etu = "tv_etu"
        
        self.ds = self.LireDS()
    
        
    def select_from_etu(self, *etc) :
        return self.select_from(self.table_etu, *etc)
    
    def questions(self) :
        return self.ds[0]
    
    def items(self) :
        return self.ds[1]
        

    def commit(self) :
        return self.bdd.commit()
    
    def maybe_commit(self) :
        if self.commit_note :
            return self.bdd.commit()

        
    def LireDS(self) :
        """Récupère la structure d'un DS et renvoie la liste des questions et des items, dans le même ordre"""
        lignes = self.select_from_struct("*", "ORDER BY question COLLATE ORDONNER_QUEST, rowid").fetchall()
        questions = []
        items = []
        for l in lignes :
            if questions == [] or l["question"] != questions[-1] :
                questions.append(l["question"])
                items.append([])
            items[-1].append((l["item"], int(l["bareme"])))
        return questions, items
    
    
    def SelectionnerEleve(self, nom, prenom) :
        if prenom is None :
            liste = self.select_from_etu("*", "WHERE nom = ? COLLATE PREFIXE_NOMS",(nom,)).fetchall()
        else :
            liste = self.select_from_etu("*", "WHERE nom = ? COLLATE PREFIXE_NOMS AND prenom = ? COLLATE PREFIXE_NOMS", (nom, prenom)).fetchall()
        if len(liste) == 0 :
            raise AucunEleve
        if len(liste) > 1 :
            raise PlusieursEleves(liste)
        return liste[0]
        
    def SelectionnerEleveParId(self, id) :
        liste = self.select_from_etu("*", "WHERE id = ?", (id,)).fetchall()
        if len(liste) == 0 :
            raise AucunEleve
        if len(liste) > 1 :
            raise PlusieursEleves(liste)
        return liste[0]
    
    def ItemsQuestion(self, q_nb) :
        return self.ds[1][q_nb]
    
    def NomQuestion(self, q_nb) :
        return self.ds[0][q_nb]
        
    def AideQuestion(self, q_nb) :
        res = self.select_from_aide("aide", "WHERE question=?", (self.NomQuestion(q_nb),)).fetchall()
        if len(res) > 0 and res[0]["aide"] is not None :
            return  res[0]["aide"]
        else :
            return ""
    
    def gerer_signe(self, s, op, repq, nq_courante) :
        if nq_courante is None :
            raise ValueError
        nsigne = repq.count(s)
        if nsigne != len(repq) or op(nq_courante, nsigne) not in range(0, len(self.ds[0])) :
            raise ValueError
        return op(nq_courante, nsigne)
    
    def QuestionNommee(self, repq, nq_courante) :
        if repq[0] == "-" :
            return self.gerer_signe("-", lambda x,y:x-y, repq, nq_courante)
        elif repq[0] == "+" :
            return self.gerer_signe("+", lambda x,y:x+y, repq, nq_courante)
        else :
            return self.ds[0].index(repq)
        
    def NombreQuestions(self) :
        return len(self.ds[0])
    
    def AncienneNote(self, eleve, q_nb) :
        """Récupère la note éventuellement déjà inscrite pour cette question, en vue de son affichage"""
        note = self.select_from_result("item, points", "WHERE id = ? AND question = ? ORDER BY rowid", (eleve["id"], self.NomQuestion(q_nb))).fetchall()
        if len(note) == 0 :
            return None
        else :
            return "/".join([x["item"] + "=" + str(x["points"]) for x in note])
    
                
    def EffacerNote(self, id, q_nb) :
        question = self.NomQuestion(q_nb)
        self.bdd.execute("DELETE FROM "+ self.table_result + " WHERE id = ? AND question = ?",(id, question))
        self.commit() 
    
    def EffacerToutesNotes(self, id) :
        self.bdd.execute("DELETE FROM "+ self.table_result + " WHERE id = ?",(id,))
        self.commit() 
    
    def EnregistrerNote(self, id, q_nb, note, replace) :
        """Effectue l'inscription d'une note dans la BDD. En cas de conflit (note déjà existante), lève une exception si replace est False.
        Renvoie le fait qu'on a réussi l'inscription et le fait qu'un remplacement a été effectué."""
        les_items = self.ItemsQuestion(q_nb)
        question = self.NomQuestion(q_nb)
        if len(note) != len(les_items) :
            raise MauvaisNbItems
        if not ItemsOK(les_items, note) :
            raise ItemTropGrand
        if replace :
            self.EffacerNote(id, q_nb)
        try :
            self.bdd.executemany("INSERT OR ROLLBACK INTO " + self.table_result + " (id, question, item, points) VALUES (?, ?, ?, ?)", genNotes(id, question, les_items, note))
            self.maybe_commit()
            return True, replace
        except self.bdd.IntegrityError :
            raise NoteExistante
    
    def NoteTotale(self, id) :
        return self.select_from_notes("note", "WHERE id = ?", (id,)).fetchone()["note"]
    
    def EnregistrerCommentaire(self, id, texte) :
        if texte == "" :
            self.EffacerCommentaire(id)
        else :
            self.bdd.execute("INSERT OR REPLACE INTO " + self.table_copie + " VALUES (?, ?)", (id, texte))
            self.maybe_commit()
    
    def EnregistrerAide(self, q_nb, texte) :
        self.bdd.execute("INSERT OR REPLACE INTO " + self.table_aide + " VALUES (?, ?)", (self.NomQuestion(q_nb), texte))
        self.maybe_commit()
    
    def AugmenterCommentaire(self, id, texte) :
        anc_comm = self.AncienCommentaire(id)
        if anc_comm is None or anc_comm == ""  :
            self.EnregistrerCommentaire(id, texte)
        else :
            self.EnregistrerCommentaire(id, anc_comm + " " + texte)
        
    def AncienCommentaire(self, id) :
        res = self.bdd.execute("SELECT commentaire FROM " + self.table_copie + " WHERE id = ?", (id,)).fetchall()
        if len(res) == 0 :
            return None
        else :
            return res[0]["commentaire"]
            
    def EffacerCommentaire(self, id) :
        self.bdd.execute("DELETE FROM " + self.table_copie + " WHERE id = ?", (id,))
        self.maybe_commit()
        
    def ParseCommentaire(self, id, texte) :
        if texte == "" or texte == "*" :
            return
        if texte == "****" :
            self.EffacerCommentaire(id)
        else :
            if texte[-1] not in ".?!" :
                texte = texte + "."
            if texte[0] == "*" :
                self.EnregistrerCommentaire(id, texte[1:])
            else :
                self.AugmenterCommentaire(id, texte)
            
    def AQCM(self) :
        return len(self.bdd.execute("SELECT * FROM sqlite_master WHERE type=? AND name=?", ("table", self.table_qcm)).fetchall()) > 0
            
    def EnregistrerRepQCM(self, id, lreponses, replace) :
        if replace :
            self.bdd.execute("DELETE FROM " + self.table_qcmrep + " WHERE id=?", (id,))
        try :
            self.bdd.executemany("INSERT OR ROLLBACK INTO " + self.table_qcmrep + " (id, quest, reponse) VALUES(?, ?, ?);", [(id, q+1, "".join(lreponses[q])) for q in range(len(lreponses))])
            self.commit()
        except self.bdd.IntegrityError :
            raise NoteExistante
    
    def LireToutesRepQCM(self) :
        return self.bdd.execute("SELECT * FROM " + self.table_qcmrep).fetchall()
    
    def EnregistrerPointsQCM(self, id, quest, points) :
        ancien = self.select_from(self.table_qcmrep, "points", "WHERE id=? AND quest=?", (id, quest)).fetchone()[0]
        if points != ancien :
            self.bdd.execute("UPDATE " + self.table_qcmrep + " SET points=? WHERE id = ? AND quest = ?", (points, id, quest))
        
    def EnregistrerTotalPointsQCM(self) :
        self.commit()
        t = self.select_from_qcm("SUM(coeff)", "").fetchone()[0]
        if t is None :
            totalQCM = 0
        else :
            totalQCM = int(t)
        self.bdd.insert_or_replace_if_different(self.table_struct, [("question", "qcm"), ("item", "qcm")], [("bareme", totalQCM)])
        self.commit()
        for id in [int(x["id"]) for x in self.select_from_etu("id", "").fetchall()] :
            points_eleve = self.select_from(self.table_qcmrep, "SUM(points)", " WHERE id=?", (id,)).fetchone()[0]
            self.bdd.insert_or_replace_if_different(self.table_result, [("id", id), ("question", "qcm"), ("item", "qcm")], [("points", points_eleve)])
        self.commit()
