import sqlite3
import unicodedata
import math
import os

def sans_accent(s) :
    """Normalise une chaine"""
    return unicodedata.normalize("NFKD", s).encode("ASCII", "ignore").decode("utf-8").upper()

def ordonner_nom(s1, s2) :
    """Relation d'ordre pour la BDD"""
    t1 = sans_accent(s1)
    t2 = sans_accent(s2)
    if t1 < t2 :
        return -1
    elif t1==t2 :
        return 0
    else :
        return 1

class StringOrInt(str) :
    def __lt__(self, other) :
        if self.isdigit() and other.isdigit() :
            return int(self) < int(other)
        elif not self.isdigit() and not other.isdigit() :
            return super().__lt__(other)
        elif self.isdigit() :
            return True
        else :
            return False

def multiSplit(s, lSep) :
    res = []
    debut = 0
    for i in range(0, len(s)) :
        if s[i] in lSep :
            res.append(s[debut:i])
            debut = i+1
    res.append(s[debut:])
    return res

def ordonner_question(t1, t2) :
    """Relation d'ordre pour la BDD"""
    if t1[0] == "q" and t2[0] != "q" :
        return -1
    if t1[0] != "q" and t2[0] == "q" :
        return 1
    if t1[0] != "q" and t2[0] != "q" :
        if len(t1) < len(t2) :
            return -1
        elif len(t1) > len(t2) :
            return +1
        elif t1 < t2 :
            return -1
        elif t1 > t2 :
            return +1
        else :
            return 0
    if t1 == "qcm" :
        return 1
    elif t2 == "qcm" :
        return -1
    q1 = [StringOrInt(x) for x in multiSplit(t1[1:],(".","-"))]
    q2 = [StringOrInt(x) for x in multiSplit(t2[1:],(".","-"))]
    if q1 < q2 :
        return -1
    if q2 < q1 :
        return 1
    return 0


def prefixe_nom(s1, s2) :
    """Relation d'ordre utilisée seulement pour sélectionner un nom d'élève"""
    t1 = sans_accent(s1.strip()).lower().replace(" ", "")
    t2 = sans_accent(s2.strip()).lower().replace(" ", "")
    if t2 == "" :
        return 1
    if t1.startswith(t2) :
        return 0
    else :
        return 1

def str_round(x, ndigits) :
    if x is None :
        return None
    else :
        return ("{0:." + str(ndigits) + "f}").format(x)

class StdevFunc:
    """Fonction d'agrégation pour l'écart-type"""
    def __init__(self):
        self.M = 0.0
        self.S = 0.0
        self.k = 0

    def step(self, value):
        if value is None:
            return
        tM = self.M
        self.k += 1
        self.M += (value - tM) / self.k
        self.S += (value - tM) * (value - self.M)


    def finalize(self):
        if self.k < 1:
            return None
        return math.sqrt(self.S / (self.k))

class AnonRow(sqlite3.Row) :
    def __getitem__(self, key) :
        if key in ("nom", "prenom") :
            return "---"
        return super().__getitem__(key)

class BDD_SQLite (sqlite3.Connection):

    IntegrityError = sqlite3.IntegrityError
    OperationalError = sqlite3.OperationalError

    # @property
    # def IntegrityError(self) :
    #     return _IntegrityError

    def __init__(self, nom_fichier, anon=False) :
        """Ouvre une bdd sqlite et met en place les relations d'ordre et agrégations ci-dessus définies"""
        os.makedirs(os.path.dirname(nom_fichier), exist_ok=True)
        super().__init__(nom_fichier)
        self.row_factory = AnonRow if anon else sqlite3.Row
        self.create_collation("PREFIXE_NOMS", prefixe_nom)
        self.create_collation("ORDONNER_NOMS", ordonner_nom)
        self.create_collation("ORDONNER_QUEST", ordonner_question)
        self.create_aggregate("STDEV", 1, StdevFunc)
        self.create_function("STRROUND", 2, str_round)

    @staticmethod
    def getKeys(cur) :
        """Récupère les clés (champs) d'un curseur bdd, même s'il n'y a aucun enregistrement"""
        return [x[0] for x in cur.description]

    def insert_or_replace_if_different(self, table, keys, values) :
        """Comme INSERT OR REPLACE mais ne fait la mise à jour que si les nouvelles valeurs sont différentes des anciennes.
        keys est une liste de couples (nom_champ, valeur) qui doit correspondre à une clé de la table.
        values est du même type est indique les valeurs à éventuellement modifier."""
        allValues = keys + values
        l_oldValues = self.execute("SELECT " + ",".join([x[0] for x in values]) + " FROM " + table + " WHERE " + " AND ".join([x[0] + "=?" for x in keys]), [x[1] for x in keys]).fetchall()
        if len(l_oldValues) > 1 :
            raise IntegrityError
        if len(l_oldValues) == 0 :
            self.execute("INSERT INTO " + table + "(" + ",".join([x[0] for x in allValues]) + ") VALUES (" + ",".join(["?" for x in allValues]) + ");", [x[1] for x in allValues])
        else :
            oldValues = l_oldValues[0]
            i = 0
            while i < len(values) and values[i][1] == oldValues[values[i][0]] :
                i = i + 1
            if i < len(values) :
                self.execute("UPDATE " + table + " SET " + ",".join([x[0]+"=?" for x in values]) + " WHERE " + " AND ".join([x[0]+"=?" for x in keys]), [x[1] for x in values+keys])