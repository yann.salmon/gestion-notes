from moteur import DS, NoteExistante
from tech import question

class SortieEleve(Exception) :
    pass

import enum

class ActionNote(enum.Enum) :
    Delete = 1

class SortieQuestion(Exception) :
    pass


class SaisieCommentaire(Exception) :
    def __init__(self, texte) :
        self.texte = texte
        
class SaisieAide(Exception) :
    def __init__(self, texte) :
        self.texte = texte

class AbandonCopie(Exception) :
    pass
    
class AbandonDebut(AbandonCopie) :
    pass

class AbandonMilieu(AbandonCopie) :
    def __init__(self, id) :
        self.id = id

class ActionDoublon(enum.Enum) :
    Demander = 0
    Ignorer = 1
    Ecraser = 2
    Completer = 3



def intornone(ch) :
    ch = ch.strip()
    if ch == "" or ch.lower() == "none" :
        return None
    else :
        return int(ch)

class InterfaceTexte :
    
    def __init__(self, args) :
        self.ds = DS(args)


    def NoteTotale(self, eleve) :
        """Totalise les points de la copie et affiche la note"""
        try :
            print("Note :", self.ds.NoteTotale(eleve["id"]))
        except :
            print("Erreur inattendue lors de la récupération du total")

    
    
    def NbCopies(self) :
        try :
            print("Vous avez corrigé", self.ds.select_from_result("COUNT(DISTINCT id)", "WHERE question <> 'qcm'").fetchone()[0], "copies")
        except :
            pass
    
    def ParseNote(self, chaine) :
        """Traite une saisie visant à attribuer des points à une question.
        La chaine vide ou réduite à - passent à la question suivante sans rien faire.
        La chaine --- interrompt la correction de la copie.
        Si la chaine est de la forme qX:, fait passer la correction à la question qX.
        Si la chaine est de la forme qX:n, fait passer la correction à la question qX et tente directement d'attribuer la note n à cette question.
        Si la chaine est de la forme n, tente d'attribuer la note n à la question courante.
        Si la question comporte plusieurs items, on doit saisir tous les items en séparant par des /. Éventuellement, si des items sont non répondus, on met // pour signifier qu'on saute cet item.
        Précéder la note de ! permet d'éviter la confirmation d'écrasement.
        La fonction renvoie le fait qu'on ait demandé le remplacement d'une note existante, la question pour laquelle on souhaite inscrire une note et la note à inscrire."""
        if chaine == "" or chaine == "-" :
            return False, None, None
        if chaine == "---" :
            raise SortieQuestion
        if chaine[0] == "*" :
            raise SaisieCommentaire(chaine[1:])
        if chaine[0] == "µ" :
            raise SaisieAide(chaine[1:])
        if ":" in chaine :
            (question, ch_note) = chaine.split(":")
        else :
            (question, ch_note) = (None, chaine)
        if question is not None :
            question = question.strip()
            if question == "" :
                raise ValueError
        ch_note = ch_note.strip()
        if ch_note == "" :
            return (False, question, None)
        if ch_note == "!-" :
            return (True, question, ActionNote.Delete)
        if ch_note[0] == "!" :
            replace = True
            ch_note = ch_note[1:]
        else :
            replace = False
        return (replace, question, [intornone(ch) for ch in ch_note.strip().split('/')])
            
    
class InterfaceAuto(InterfaceTexte) :
    def __init__(self, args) :
        super().__init__(args)
        self.ds.commit_note = False
        self.action_doublon = ActionDoublon(args.doublon)
        self.f = open(nom_fichier)
        
    def QueFaireDoublon(self) :
        if self.action_doublon is not ActionDoublon.Demander :
            print("Copie déjà présente ; action prise :", self.action_doublon.name)
            return self.action_doublon
        reponse = question("Copie déjà présente ; ignorer, écraser, completer ? ",["i", "e", "c"], False, "(majuscule pour le faire par défaut à l'avenir) ")
        action = ActionDoublon(["i", "e", "c"].index(reponse.lower()) + 1)
        if reponse.isupper() :
            self.action_doublon = action
        return action
        
    def InsereNote(self, id, q_nb, note, replace) :
        """Effectue l'inscription d'une note dans la BDD. En cas de conflit (note déjà existante), demande confirmation si replace est False.
        Renvoie le fait qu'on a réussi l'inscription."""
        try :
            succes, repl  = self.ds.EnregistrerNote(id, q_nb, note, replace)
            if repl :
                print("Remplacement de la note en question", self.ds.QuestionNommee(q_nb))
            return succes
        except NoteExistante :
            print("Erreur d'intégrité en question", self.ds.QuestionNommee(q_nb))
            print("Abandon de la copie")
            raise AbandonMilieu(id)
            
    def Corriger(self) :
        try :
            while True :
                try :
                    self.TraiterCopie()
                except AbandonDebut :
                    print("Abandon de la copie dès le début")
                except AbandonMilieu as e :
                    print("Abandon au milieu de la copie de l'élève", e.id)
                    reponse = question("Voulez-vous effacer toutes ses notes ?", ["o", "n"])
                    if reponse == "o" :
                        self.ds.EffacerToutesNotes(e.id)
                        self.ds.EffacerCommentaire(e.id)
        except SortieEleve :
            self.NbCopies()
            self.ds.commit()
            self.ds.close()
            self.f.close()
