from moteur import PlusieursEleves, AucunEleve, NoteExistante, MauvaisNbItems, ItemTropGrand
from tech import question, editerTexte

from interf_interact import *

import tempfile
import subprocess
import os



class InterfaceSerie(InterfaceInteract) :
    
    def __init__(self, args) :
        super().__init__(args)
    
    def Corriger(self) :
        try :
            q_nb = 0
            while q_nb < self.ds.NombreQuestions() :
                q_nb, reussi = self.InputQuestion(q_nb)
                if reussi :
                    self.TraiterQuestion(q_nb)
                    q_nb = q_nb + 1
        except SortieQuestion :
            self.NbCopies()
            self.ds.commit()
            self.ds.close()

    def InputQuestion(self, q_nb) :
        try :
            prompt = "Question à traiter ? (actuellement " + self.ds.NomQuestion(q_nb) + ", --- pour quitter) "
            repq = input(prompt)
            if repq == "" :
                return q_nb, True
            if repq == "---" :
                raise SortieQuestion
            q_nb = self.ds.QuestionNommee(repq, q_nb)
            return q_nb, True
        except ValueError :
            print("Question "+ repq + " introuvable")
            return q_nb, False

    
    def TraiterEleve(self, eleve, q_nb) :
        """Gère le processus de saisie d'une note pour une question, c'est-à-dire qu'on signale les saisies incorrectes détectées par les fonctions auxiliaires, qu'on vérifie que la question demandée existen qu'on a rentré le bon nombre d'items, que les notes sont possibles et tente l'inscription effective de cette note.
        Renvoie le fait qu'on a réussi ce qu'on voulait, ce qui permet à la fonction appelante de savoir si elle doit passer à la question suivante ou rester sur celle-ci."""
        try :
            (replace, repq, note) = self.InputNote(eleve, q_nb)
        except ValueError :
            print("Erreur de saisie")
            return q_nb, False
        except SaisieCommentaire :
            self.InputCommentaire(eleve["id"], True)
            return q_nb, False
        if repq is None :
            question = self.ds.NomQuestion(q_nb)
        else :
            raise SortieEleve
        if note is None :
            return q_nb, repq is None
        if note == ActionNote.Delete :
            try :
                self.ds.EffacerNote(eleve["id"], question)
                return q_nb, True
            except :
                print("Impossible d'effacer la note pour la question", question)
                return q_nb, False
        try :
            return q_nb, self.InsereNote(eleve["id"], q_nb, note, replace)
        except MauvaisNbItems :
            print ("Mauvais nombre d'items pour la question " + question)
            return q_nb, False
        except ItemTropGrand :
            print ("Valeur trop élevée pour un item en question "+ question)
            return q_nb, False
            
    
    def TraiterQuestion(self, q_nb) :
        try :
            while True :
                eleve = self.InputEleve()
                print("Copie de ", list(eleve))
                self.TraiterEleve(eleve, q_nb)
        except SortieEleve :
            pass

